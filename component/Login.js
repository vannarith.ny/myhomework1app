import React, {Component} from 'react';
import {Text, View, StyleSheet, TextInput, TouchableOpacity, Image, CheckBox} from 'react-native';
import { gray } from 'ansi-colors';

export default class Login extends Component {
  

  render() {
    const { containerStyle, textStyle, inputStyle, buttonStyle, imageStyle, containerLoginStyle, labelText, containerFooter, rememberMe, btnLogin, checkboxStyle } = styles
    return (
      <View style={containerStyle}>
        <Image
            style={imageStyle} 
            source={require('../img/w-logo-blue.png')}
            />
        <View style={containerLoginStyle}>
            <Text style={labelText}> Username or Email Address </Text>
            <TextInput
                style={inputStyle}
            />

            <Text style={labelText}> Password </Text>
            <TextInput
                style={inputStyle}
                secureTextEntry={true}
            />

            <View style={containerFooter}>
                <View style={rememberMe}>
                    <CheckBox style={checkboxStyle}></CheckBox>
                    <Text style={labelText}> Remember Me </Text>
                </View>
                <View style={btnLogin}>
                    <TouchableOpacity
                        style={buttonStyle}
                        onPress = {this.handlebutton}
                    >
                        <Text> Login </Text>
                    </TouchableOpacity>
                </View>
            </View>
           
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    flex: 1,
    fontFamily: 'Helvetica Neue',
  },
  containerLoginStyle: {
    width: '90%',
    borderWidth: 1,
    borderColor: '#DDDDDD',
    backgroundColor: '#FFF',
    textAlign: 'left',
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20,
    marginTop: 20,
  },
  containerFooter: {
    width: '100%',
    fontSize:18,
    paddingTop: 20,
    flexDirection: 'row'
  },
  textStyle: {
    fontSize: 20,
    textAlign: 'center',
    fontStyle: 'italic',
    fontWeight: 'bold'
  },
  inputStyle: {
    height: 45,
    borderWidth: 1,
    borderColor: 'gray',
    fontSize: 20,
    borderRadius: 2,
    width: '100%',
    marginTop: 10,
    backgroundColor: '#fbfbfb',
  },
  buttonStyle: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    marginLeft: '20%',
    marginRight: '20%',
    marginTop: 20
  },
  imageStyle: {
    width: 100,
    height: 100
  },
  labelText: {
      fontSize:16,
      color: '#72777c',
      paddingTop: 20,
  },
  rememberMe: {
      width: '50%',
      flexDirection: 'row',
      
  },
  checkboxStyle: {
    marginTop: 15,
  },
  btnLogin: {
      width: '50%',
  }
})


